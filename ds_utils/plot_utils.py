import pandas
import pandas as pd
import numpy as np
from matplotlib import pyplot as plt
from sklearn.metrics import roc_curve

def get_plot_instance(figsize=(10, 10), subplots=(1, 1)):
    f, ax = plt.subplots(*subplots, figsize=figsize)
    return f, ax

def set_figure_and_axes(f, ax):
    ax.grid(True)
    ax.set_facecolor('0.8')
    f.set_facecolor('0.7')
    return f, ax
    
# Sklearn and lightgbm plotter
def plot_roc_curve(x_train, x_test, y_train, y_test, clf, figsize=None, set_style=True, savename='blank'):
    from sklearn.base import ClassifierMixin
    from lightgbm import Booster
    assert x_train.shape[1] == x_test.shape[1], 'The number of features in train and test sets does not match. Make sure that the dataframes are correct'
    assert y_train.nunique() == 2 and y_test.nunique() == 2, 'The taget is not binary or the number of target levels in train and test sets does not match'
    assert isinstance(clf, ClassifierMixin) == 1 or isinstance(clf, Booster) == 1, 'CLF is not a valid classifier. Should be ClassifierMixin or Booster'
    assert isinstance(figsize, tuple) == 1 or isinstance(figsize, list) == 1 or figsize is None, 'FIGSIZE is not a valid object. Should be a tuple, a list or None'
    
    if figsize is None:
        f, ax = get_plot_instance()
    else:
        f, ax = get_plot_instance(figsize=figsize)
        
    if isinstance(clf, ClassifierMixin):
        y_score_train = clf.predict_proba(x_train)[:, 1]
        y_score_test = clf.predict_proba(x_test)[:, 1]
    else:
        y_score_train = clf.predict(x_train)
        y_score_test = clf.predict(x_test)
    
    fpr, tpr, thresholds = roc_curve(pos_label=1, y_true=y_train, y_score=y_score_train)
    ax.plot(fpr, tpr, label='Train')
    fpr, tpr, thresholds = roc_curve(pos_label=1, y_true=y_test, y_score=y_score_test)
    ax.plot(fpr, tpr, label='Test')
    ax.plot([0, 1], [0, 1], label='baseline')
    ax.legend(loc='best')
    ax.set_xlim([0, 1])
    ax.set_ylim([0, 1])
    if set_style:
        f, ax = set_figure_and_axes(f, ax)
    f.show()
    if savename != 'blank':
        f.savefig('./src/out/{}'.format(savename))
        
def get_lift_chart(df, target_name, proba_name, buckets_num=10):
    loc_df = df.sort_values(by=proba_name, ascending=False)
    loc_df['bucket'] = pd.qcut(x=loc_df[proba_name].rank(method='first', ascending=False), q=10, labels=False) + 1
    total_hits = loc_df[target_name].sum()
    lift = loc_df.groupby('bucket')[target_name].sum().reset_index(name='hit_per_bucket')
    lift['lift'] = lift['hit_per_bucket']/total_hits * 100
    lift['cum_hit_per_bucket'] = lift['hit_per_bucket'].cumsum()
    lift['cum_lift'] = lift['cum_hit_per_bucket']/total_hits * 100
    return lift


def plot_lifts(df):
    f, ax = plt.subplots(1, 2, figsize=(18, 8))
#     ax_2 = f.add_subplot(1, 2, 2)
    ax[0].plot(df['bucket']*10, df['lift'], label='Lift')
    ax[1].plot(df['bucket']*10, df['cum_lift'], label='Cumulative lift')
    ax[1].plot([0, 100], [0, 100], '--', label='Random')
    ax[0].legend(loc='best')
    ax[1].legend(loc='best')
    ax[0].grid(True)
    ax[1].grid(True)
    ax[0].set_facecolor('0.8')
    ax[0].set_facecolor('0.8')
    f.show()

def plot_crossval_score(train_auc, test_auc, savename='blank', title='No title', figsize=(12, 8), savefig=False):
    f, ax = get_plot_instance(figsize=figsize)
    ax.plot(list(range(len(train_auc))), train_auc, label='Train i-th fold score')
    ax.plot(list(range(len(test_auc))), test_auc, label='Test i-th fold score')
    ax.grid(True)
    ax.set_facecolor('0.8')
    ax.plot(list(range(len(train_auc))), [np.array(train_auc).mean()]*len(train_auc), '--', label='Train mean reference')
    ax.plot(list(range(len(test_auc))), [np.array(test_auc).mean()]*len(test_auc), '-.', label='Test mean reference')
    ax.set_xlabel('Fold number')
    ax.set_ylabel('Cross validation scores')
    ax.set_title(title)
    ax.legend(loc='best')
    f.set_facecolor('0.7')
    f.show()
    if savefig:
        f.savefig(savename)
    
def plot_confusion_matrix(cm, classes,
                          normalize=False,
                          title='Confusion matrix',
                          cmap=plt.cm.Blues):
    """
    This function prints and plots the confusion matrix.
    Normalization can be applied by setting `normalize=True`.
    """
    if normalize:
        cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]
        print("Normalized confusion matrix")
    else:
        print('Confusion matrix, without normalization')

    print(cm)

    plt.imshow(cm, interpolation='nearest', cmap=cmap)
    f = plt.gcf()
    f.set_facecolor('0.7')
    plt.title(title)
    plt.colorbar()
    tick_marks = np.arange(len(classes))
    plt.xticks(tick_marks, classes, rotation=45)
    plt.yticks(tick_marks, classes)

    fmt = '.2f' if normalize else 'd'
    thresh = cm.max() / 2.
    for i, j in itertools.product(range(cm.shape[0]), range(cm.shape[1])):
        plt.text(j, i, format(cm[i, j], fmt),
                 horizontalalignment="center",
                 color="white" if cm[i, j] > thresh else "black")

    plt.tight_layout()
    plt.ylabel('True label')
    plt.xlabel('Predicted label')