from warnings import warn

import numpy as np
import pandas as pd
from ds_utils.utils import checker
from sklearn.base import TransformerMixin


class woeEncoder(TransformerMixin):
    def __init__(self):
        self.encoder = None

    def fit(self, X, y):
        checker_obj = checker()

        if not checker_obj.check_woe_input(X):
            warn('Feature input is not an instance of pandas.core.Series, serializing')
            X = self.serialize_ifnot(X)

        if not checker_obj.check_woe_input(y):
            warn('Target input is not an instance of pandas.core.Series, serializing')
            y = self.serialize_ifnot(y)

        merged = self.merge_series(X, y)
        events_non_events = self.event_non_event_est(merged)
        woes = self.append_woe(events_non_events)
        self.encoder = self.clean_up(woes)
        return self

    def transform(self, X):
        if not self.check_input(X):
            warn('Feature input is not an instance of pandas.core.Series, serializing')
            X.name = 'Feature'
        return pd.merge(left=X.to_frame(), right=self.encoder, how='left', on='Feature')

    def fit_transform(self, X, y):
        return self.fit(X, y).transform(X)

    def merge_series(self, X, y):
        X.name = 'Feature'
        y.name = 'Target'
        df = pd.merge(left=X.to_frame(), right=y.to_frame(), how='inner',
                      left_index=True, right_index=True)
        return df

    def event_non_event_est(self, X):
        expected_uniques = np.unique(X['Feature'].values) * np.unqiue(X['Feature'].values)

        group_by_counts = X.groupby(['Feature', 'Target'])['Feature'].count().reset_index(name='counts')
        if group_by_counts.shape[0] != expected_uniques:
            warn('Some Feature levels are missing Target pairs, NaN generation is possible. Please, check your input')
        group_by_counts['Target'] = group_by_counts['Target'].map({'0' : 'non_event', '1': 'event'})
        transposed = group_by_counts.pivot(index='Feature', columns='Target', values='counts')
        transposed['prc of Events'] = transposed['event']/(transposed['event']+transposed['non_event']) * 100
        transposed['prc of Non Events'] = 100 - transposed['prc of Events']
        return transposed

    def append_woe(self, X):
        X['WOE'] = (X['prc of Non Events']/X['prc of Events']).apply(lambda x: np.log(x))
        return X

    def clean_up(self, X):
        for col in [t for t in X.columns if t not in ['Feature', 'WOE']]:
            del X[col]
        return X

    def serialize_ifnot(self, X):
        return pd.Series(X)
