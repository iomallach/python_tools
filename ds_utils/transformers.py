class TargetEncoder(object):
	def __init__(self, min_samples_leaf=1, smoothing=1, noise_level=0):
		self.min_samples_leaf = min_samples_leaf
		self.smoothing = smoothing
		self.noise_level = noise_level
	
	def check_series(self, series):
		assert len(series) == len(self.trn_series)
		assert self.trn_series_name == series.name
	
	def fit(self, trn_series, target):
		temp = pd.concat([trn_series, target], axis=1)
		self.trn_series_name = trn_series.name
		self.target_name = target.name
		# Compute target mean 
		self.averages = temp.groupby(by=trn_series.name)[target.name].agg(["mean", "count"])
		# Compute smoothing
		self.smoothing = 1 / (1 + np.exp(-(self.averages["count"] - self.min_samples_leaf) / self.smoothing))
		# Apply average function to all target data
		self.prior = target.mean()
		# The bigger the count the less full_avg is taken into account
		self.averages[target.name] = self.prior * (1 - self.smoothing) + self.averages["mean"] * self.smoothing
		self.averages.drop(["mean", "count"], axis=1, inplace=True)
		self.trn_series_index = trn_series.index
		return self
	
	def add_noise(self, series):
		return series * (1 + self.noise_level * np.random.randn(len(series)))
	
	def transform(self, series):
		ft_series = pd.merge(
        series.to_frame(series.name),
        self.averages.reset_index().rename(columns={'index': self.target_name, self.target_name: 'average'}),
        on=series.name,
        how='left')['average'].rename(self.trn_series_name + '_mean').fillna(self.prior)
		ft_series.index = series.index
		return self.add_noise(ft_series)
	
	def fit_transform(self, trn_series, target):
		return self.fit(trn_series, target).transform(trn_series)


class ProportionalResampler(object):
    '''
        Parameters:
            event_draw_fraction - what fraction of events to draw from X, a float from 0 to 1. Default: 1
            event_out_fraction - a fraction of events in a resampled DataFrame, a float from 0 to 1. Default: 0.5
            strata - a binary variable used to stratify resampling, an integer with values in [0, 1]. Default: 'target'
            random_state - a resample random state. Default: 42
            
            Example:
            X = pd.readcsv('./myfiles/mycsv.csv')
            sampler = ProportionalResampler(event_draw_fraction=1, event_out_fraction=0.05, strata='had_application', random_state=0)
            resampled_dataframe = sampler.fit_sample(X)
    '''
    def __init__(self, event_draw_fraction=1, event_out_fraction=0.5, strata='target', random_state=42):
        self.event_draw_fraction = event_draw_fraction
        self.event_out_fraction = event_out_fraction
        self.strata = strata
        self.random_state = random_state
        
    def fit(self, X):
        assert isinstance(X, pandas.core.frame.DataFrame), 'Input object is not a DataFrame instance'
        counts = X[self.strata].value_counts()
        self.event_draw_count =int( counts[1] * self.event_draw_fraction)
        self.non_event_draw_count = int(self.event_draw_count * (1 - self.event_out_fraction)/self.event_out_fraction)
        return self
    
    def sample(self, X):
        assert isinstance(X, pandas.core.frame.DataFrame), 'Input object is not a DataFrame instance'
        return pd.concat([X.query('{} == 0'.format(self.strata)).sample(n=self.non_event_draw_count, random_state=self.random_state),
                          X.query('{} == 1'.format(self.strata)).sample(n=self.event_draw_count, random_state=self.random_state)])
    
    def fit_sample(self, X):
        return self.fit(X).sample(X)


class SelectFromModelPd(object):
    def __init__(self, clf, n_features, threshold):
        self.clf = clf
        if threshold is None:
            self.n_features = n_features
            self.n_features_flag = 1
        else:
            self.threshold = threshold
            self.n_features_flag = 0
            
    def fit(self, X):
        if self.n_features_flag:
            self.top_features = self.select_with_n_features(X)
        else:
            self.top_features = self.select_with_threshold(X)
        return self
    
    def select_with_threshold(self, X):
        return pd.Series(self.clf.features_importances_, index=X.columns.values).loc[lambda x: x >= self.threshold].index
    
    def select_with_n_features(self, X):
        return pd.Series(self.clf.features_importances_, index=X.columns.values).nlargest(self.n_features).sort_values(ascending=False).index
    
    def transform(self, X):
        return X.loc[:, self.top_features]
    
    def fit_transform(self, X):
        return self.fit(X).transform(X)


def add_date_features(X, date_vars):
    for date_var in [date_vars] if isinstance(date_vars, str) else date_vars:
        X['dayofweek_{}'.format(date_var)] = X[date_var].dt.dayofweek
        X['month_{}'.format(date_var)] = X[date_var].dt.month
        X['year_{}'.format(date_var)] = X[date_var].dt.year
        X['quarter_{}'.format(date_var)] = X[date_var].dt.qtr
        


# Category embedder in development
from tensorflow import set_random_seed
set_random_seed(42)
from keras.models import Model, Sequential
from keras.layers import Input, Dense, Concatenate, Reshape, Dropout, Merge
from keras.layers.embeddings import Embedding
from keras import regularizers


def get_cat_vars_dict(x):
    return {c: list(x[c].unique()) for c in x.select_dtypes(include='object').columns}


def get_embed_cols(x, verbose=True):
    embed_cols = []
    for c in x:
        if len(x[c]) > 2:
            embed_cols.append(c)
            if verbose:
                print(c + ': %d values' % len(col_vals_dict[c]))
    return embed_cols

def get_nunique_cat(x, cats):
    nuniques = {}
    for c in cats:
        nuniques[c] = x[c].nunique()
    return nuniques


class CategoryEmbedder(object):
    def __init__(self, embed_cols, nuniques, n_dense, n_dense_params):
        self.full_model = Sequential()
        self.embed_cols = embed_cols
        self.nuniques = nuniques
        self.n_dense = n_dense
        self.n_dense_params = n_dense_params

    def build_model(self, embed_penalty, embed_penalty_val, learning_rate):
        models = []
        self.embed_outsize = 0

        for c in self.embed_cols:
            model = Sequential()
            no_of_unique_cat = self.nuniques[c]
            embedding_size = int(min(np.ceil(no_of_unique_cat/2), 50))
            self.embed_outsize += embedding_size
            vocab = no_of_unique_cat + 1
            # Регуляризатор добавь
            model.add(Embedding(vocab, embedding_size, input_length=1, name='{}_embedding'.format(c)))
            model.add(Reshape(target_shape=(embedding_size,)))
            models.append(model)
        self.full_model.add(Merge(models, mode='concat'))
        self.full_model.add(Dropout(0.6))
        self.full_model.add(Dense(20, activation='relu'))
        self.full_model.add(Dense(1, activation='sigmoid'))
        self.full_model.compile(loss='binary_crossentropy', optimizer=Adam(lr=learning_rate), metrics=['accuracy'])
        return self


    def get_summary(self):
        return self.full_model.summary()
    
    
    def get_full_model(self):
        return self.full_model
    
    def fit(self, X, y, t_t_split=True, test_size=0.2, epochs=1):
        X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=test_size)
        self.full_model.fit(X_train, y_train, epochs=epochs, validation_data=(X_test, y_test))
        return self