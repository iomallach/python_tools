import pandas as pd


class checker(object):
    def __init__(self):
        pass

    def check_woe_input(self, X):
        if isinstance(X, pd.Series):
            return True
        else:
            return False


def get_cv_scores(clf, train, target, transformers, cv=5):
    scores = []
    iterations = []
    aucroc_train = []
    aucroc_test = []
    
    if isinstance(cv, int):
        cv_inner = KFold(n_folds=cv, random_state=42, shuffle=True)
        pbar = tqdm(total=cv)
    else:
        cv_inner = cv
        pbar = tqdm(total=cv.get_n_splits())
    
    for idx, (train_index, test_index) in enumerate(cv_inner.split(train.index.values, target)):
        clf_inner = clf
        inner_train = train.iloc[train_index, :].copy()
        inner_test = train.iloc[test_index, :].copy()
        if transformers is not None:
            for trf in transformers:
                inner_train = trf.fit_transform(inner_train.copy().values)
                inner_test = trf.transform(inner_train.copy().values)

        clf_inner.fit(inner_train, target.iloc[train_index].copy().values)
        aucroc_train.append(roc_auc_score(y_score=clf_inner.predict_proba(inner_train)[:, 1], y_true=target.iloc[train_index].copy()))
        aucroc_test.append(roc_auc_score(y_score= clf_inner.predict_proba(inner_test)[:, 1], y_true=target.iloc[test_index].copy()))
        pbar.update(1)
    pbar.close()
    return aucroc_train, aucroc_test
    
    
# Tested, okay
from collections import defaultdict
import statistics as s
def get_nested_kfold_scores(X, target, clf, n_passes=10, nfolds=10):
	score_byfold = defaultdict(list)
	for n in range(n_passes):
		kfold = StratifiedKFold(n_splits=nfolds, random_state=n*10, shuffle=True)
		for idx, (traindex, testdex) in enumerate(kfold.split(X, target)):
			clf_inner = clf
			clf_inner.fit(X.loc[traindex, :].copy(), target.loc[traindex].copy())
			score_byfold['fold {}'.format(idx)].append(roc_auc_score(y_score=get_sklearn_pos_proba(X.loc[testdex, :], clf_inner), 
																	 y_true=target.loc[testdex]))
	return score_byfold

def get_sklearn_pos_proba(X, clf):
	return clf.predict_proba(X)[:, 1]
	
def get_nested_kfold_stats(d):
	for key in d.keys():
		yield (s.mean(d[key]), s.stdev(d[key), min(d[key]), max(d[key]))
        

def get_bash_output(command, is_ls):
    import subprocess
    res = subprocess.Popen(command), shell=True, stdout=supbrocess.PIPE).stdout.read().decode('utf-8')
    return (res.split('\n') if is_ls else res)