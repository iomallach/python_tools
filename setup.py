from setuptools import setup, find_packages

__version__ = '0.0.3'

setup(
    name='ds_utils',
    version=__version__,
    description='A collection of badly written data science tools',
    url='https://bitbucket.org/iomallach/python_tools',
    download_url='https://bitbucket.org/iomallach/python_tools',
    classifiers=['Intended Audience :: Science/Research',
                 'Intended Audience :: Developers',
                 'Programming Language :: Python',
                 'Topic :: Software Development',
                 'Topic :: Scientific/Engineering',
                 'Operating System :: Microsoft :: Windows',
                 'Operating System :: POSIX',
                 'Operating System :: Unix',
                 'Operating System :: MacOS',
                 'Programming Language :: Python :: 3.5',
                 'Programming Language :: Python :: 3.6'],
    keywords='python data science machine learning pandas sklearn numpy',
    packages=find_packages(),
    install_requires=['numpy', 'scipy', 'scikit-learn', 'pandas', 'matplotlib'],
    author='iomallach',
    author_email='a.butenko.o@gmail.com'
)
